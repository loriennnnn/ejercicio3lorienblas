package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("Introduce una cadena para verla en mayusculas");
		
		System.out.print("Cadena = ");
		String cadena=in.nextLine();
		
		System.out.println(cadena.toUpperCase());
		
		in.close();

	}

}
